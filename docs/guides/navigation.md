---
id: navigation
title: Navigation
---

First important thing is to navigate through your files and translations.

## Explore the words

On the left side you will find file and words explorer. It has simple structure as you can find in file explorers. You can browse files, namespaces and individual keys. You also find buttons such as add namespace and key for creating translations. Last thing you can do is managing your set of used languages.

![Translations explorer](assets/explorer.png)

Navigation is natural and it copies the directory structure of your files. Just click through it!

## Folder, namespace or key?

Maybe you are lost in naming convention here. Let's enlighten you.

> Missed how to [structure](creating-project.md) your project?

**Folder** is really straight forward. It just copies your folder structure. So if your project contains just one folder called `main` you will find it here as a top level element. As your project grows bigger your folder structure will most likely scale with it.

**Namespace** is everything in each `.json` file which is not **key**. Meaning all properties of objects in translation file that have no translation such as `"my_key": "My key"`. Simple example of namespace would be **form** in:

```json
{
    "form": {
        "input": "Input"
    }
}
```

**Key** is the property in `.json` file bound to translation. In previous example it would be **input** property.

### Controls

Buttons alongside explorer items are used for adding folders, namespaces and keys. Simple click the desired icon and fill in the key to remove/add.

### Badges

You can notice badge next to each folder and namespace. It represents missing translations in each group. All languages are counted in one number so you can see which part needs most work.

## Languages

![Languages menu](assets/languages.png)

Translations would be pointless without languages. You can control which you are using in settings at the top of explorer panel. By opening the menu you will find add/remove/edit feature for languages (which means `.json` files).

> Keep in mind that by removing language you are also removing all associated translations.
