---
id: editing
title: Editing
---

Finally we can input some translations.

## Single key

Once you select key from explorer you will end up at editing view.

![Editing key](assets/edit-key.png)

You see the key itself followed by translations in all languages. No more no less. Just translate it.

## Whole namespace

There are times you will need to see whole namespace aligned in one place. That's why you can display it in table.

![Editing namespace](assets/edit-namespace.png)

In this view translating linked items is easy and you can use features like copying and bulk insert.
